const express = require('express');
const router = express.Router();
const AccountModel = require('../model/account');
const UserModel = require('../model/user');
const TransactionsModel = require('../model/transactions');
const passport = require('passport');

router.get('/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    if (!req.params.id) {
        res.status(400).json({ success: false, message: "The id field is required" });
    }
    UserModel.getUserById(req.params.id, (err, userFound) => {
        if (err) throw err;
        if (!userFound) {
            return res.status(400).json({ success: false, message: 'User no found' });
        } else {
            AccountModel.getAccountsByUser(req.params.id, (err, accounts) => {
                if (err) throw err;
                if (!accounts) {
                    return res.status(400).json({ success: false, message: 'Accounts no found' });
                } else {
                    return res.status(200).json({ success: true, user: userFound, accounts: accounts });
                }
            });
        }
    });

});

router.post('/transactions/', passport.authenticate('jwt', { session: false }), (req, res, next) => {

    if (!req.body.account) {
        res.status(400).json({ success: false, message: "The account field is required" });
    }
    if (!req.body.amount) {
        res.status(400).json({ success: false, message: "The amount field is required" });
    }
    if (!req.body.currency) {
        res.status(400).json({ success: false, message: "The currency field is required" });
    }
    if (!req.body.operation) {
        res.status(400).json({ success: false, message: "The operation field is required" });
    }

    let transactionsInfo = TransactionsModel({
        account: req.body.account,
        amount: req.body.amount,
        currency: req.body.currency,
        operation: req.body.operation,
        status: 1
    });

    TransactionsModel.addTransactions(transactionsInfo, (err, transaction) => {
        if (err) {
            res.status(400).json({ success: false, message: 'An error occurred while trying to create the transaction, please try again' });
        } else {
            res.status(201).json({ success: true, create: transaction });
        }
    });

});

router.get('/transactions/:id', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    if (!req.params.id) {
        res.status(400).json({ success: false, message: "The id field is required" });
    }
    AccountModel.getAccountsByID(req.params.id, (err, AccountFound) => {
        if (err) throw err;
        if (!AccountFound) {
            return res.status(400).json({ success: false, message: 'Account no found' });
        } else {
            UserModel.getUserById(AccountFound.user, (err, userFound) => {
                if (err) throw err;
                if (!userFound) {
                    return res.status(400).json({ success: false, message: 'User no found' });
                } else {
                    TransactionsModel.getTransactionsByAccount(req.params.id, (err, accounts) => {
                        if (err) throw err;
                        if (!accounts) {
                            return res.status(400).json({ success: false, message: 'Transactions no found' });
                        } else {
                            return res.status(200).json({ success: true , user : userFound, account : AccountFound , transactions: accounts });
                        }
                    });
                }
            });
        }
    });
});


module.exports = router;