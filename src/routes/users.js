const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const hash = require('object-hash');
const UserModel = require('../model/user');
const keys = require('../config/keys');
const multer = require('multer');
const AWS = require('aws-sdk');

const ACCESSS_KEY_ID = 'AKIAJBYTBX736RJT4VMQ';
const SECRET_ACCESS_KEY = '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';
const BUCKET_NAME = 'bbva-attachments';

AWS.config.update({
    accessKeyId: ACCESSS_KEY_ID,
    secretAccessKey: SECRET_ACCESS_KEY,
    subregion: 'us-east-1'
});


const upload = multer({ storage: multer.memoryStorage(), limits: { fileSize: 123456789078900 } });

router.post('/aws/upload', upload.single('image'), (req, res) => {
    const Key = req.body.name + '-' + Date.now() + '.png';
    const objectParams = { Bucket: BUCKET_NAME, Key , Body: req.file.buffer, ACL: 'public-read' };
    const uploadPromise = new AWS.S3({ apiVersion: '2006-03-01' }).putObject(objectParams).promise();
    uploadPromise
    .then(() => res.status(200).json({success: true, message: 'Upload Correctly', url : `https://${BUCKET_NAME}.s3.amazonaws.com/${Key}`}))
    .catch(error => res.status(400).json({success: false, message: 'Try uploading the file again', error : error , params : objectParams }))
});


router.get('/', (req, res, next) => {
    var query = UserModel.find();
    query.exec(function (err, users) {
        if (err) {
            res.status(400).json({ success: false, message: "An error occurred.", error: err });
        } else {
            res.status(200).json({ success: true, users: users });
        }
    });
});

router.put('/', (req, res) => {
    const userUpdate = req.body
    UserModel.updateUser(userUpdate, (err, user) => {
        if (err) throw err;
        if (!user) {
            res.status(400).json({ success: false, message: 'User not found' });
        } else {
            res.status(200).json({ success: true, message: 'Updated User' });
        }
    });
});


router.put('/logout', (req, res) => {
    const userLogout = {
        "_id" : req.body.id,
        "lastLogin" : new Date(),
    }
    UserModel.updateUser(userLogout, (err, user) => {
        if (err) throw err;
        if (!user) {
            res.status(400).json({ success: false, message: 'User not found' });
        } else {
            res.status(200).json({ success: true, message: 'Logout' });
        }
    });
});

router.delete('/', (req, res) => {
    const userUpdate = req.body
    UserModel.deleteUser(userUpdate, (err, user) => {
        if (err) throw err;
        if (!user) {
            res.status(400).json({ success: false, message: 'User not found' });
        } else {
            res.status(200).json({ success: true, message: 'Deleted User' });
        }
    });
});


router.get('/search/:email', (req,res) => {
    
    if (!req.params.email) {
        res.status(400).json({ success: false, message: "The email params is required" });
    }

    let email = req.params.email;

    UserModel.getUserByEmail(email, (err, userFound) => {
        if (err) throw err;
        if (!userFound) {
            res.status(400).json({ success: false, message: 'User no found' });
        }else{
            res.status(200).json({ success: true, user: userFound });
        }
    })
 });


router.post('/create', (req, res, next) => {

    if (!req.body.name) {
        res.status(400).json({ success: false, message: "The name field is required" });
    }
    if (!req.body.lastname) {
        res.status(400).json({ success: false, message: "The lastname field is required" });
    }
    if (!req.body.identity) {
        res.status(400).json({ success: false, message: "The identity field is required" });
    }
    if (!req.body.gender) {
        res.status(400).json({ success: false, message: "The gender field is required" });
    }
    if (!req.body.email) {
        res.status(400).json({ success: false, message: "The email field is required" });
    }
    if (!req.body.password) {
        res.status(400).json({ success: false, message: "The password field is required" });
    }

    var photoUser = "";
    if (!req.body.photo) {
        photoUser = "https://www.flaticon.com/svg/static/icons/svg/172/172163.svg";
    } else {
        photoUser = req.body.photo;
    }

    let userInfo = UserModel({
        name: req.body.name,
        lastname: req.body.lastname,
        identity: req.body.identity,
        gender: req.body.gender,
        email: req.body.email,
        password: req.body.password,
        hash: hash({ foo: req.body.identity }),
        lastLogin: new Date(),
        photo: photoUser,
        status: 0
    });

    UserModel.getUserByEmail(req.body.email, (err, userDetected) => {
        if (err) {
            res.status(400).json({ success: false, message: "An error occurred.", error: err });
        }
        if (userDetected) {
            return res.status(400).json({ success: false, message: 'The email is in use.' });
        }

        UserModel.addUser(userInfo, (err, user) => {
            if (err) {
                res.status(400).json({ success: false, message: 'An error occurred while trying to register the user, please try again' });
            } else {
                res.status(201).json({ success: true, create: user });
            }
        });

    });

});


router.get('/prolife', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    res.status(200).json({ success : true, user: req.user });
});



router.post('/authenticate', (req, res, nex) => {

    if (!req.body.email) {
        res.status(400).json({ success: false, message: "The email field is required" });
    }
    if (!req.body.password) {
        res.status(400).json({ success: false, message: "The password field is required" });
    }

    const email = req.body.email;
    const password = req.body.password;

    UserModel.getUserByEmail(email, (err, userFound) => {
        if (err) throw err;
        if (!userFound) {
            return res.status(400).json({ success: false, message: 'User no found' });
        }
        UserModel.comparePassword(password, userFound.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                if (userFound.status == 0) {
                    return res.status(400).json({ success: false, message: 'User is not activated' })
                } else {

                    const token = jwt.sign({ data: userFound }, keys.secret, {
                        expiresIn: 604800 //1 Week
                    });

                    res.status(200).json({
                        success: true,
                        token: `Bearer ${token}`,
                        user: {
                            id: userFound._id,
                            name: userFound.name,
                            lastname: userFound.lastname,
                            photo: userFound.photo,
                            indentity: userFound.indentity,
                            email: userFound.correo
                        }
                    });
                }
            } else {
                return res.status(400).json({ success: false, message: 'Password is incorrect' })
            }
        });
    })
});

module.exports = router;