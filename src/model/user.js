var db = require('../config/db-connection')();
var mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');//Cliente Schema

const UserSchema = mongoose.Schema({
    name: String,
    lastname: String,
    identity: String,
    gender: String,
    email: String,
    password: String,
    hash: String,
    lastLogin: Date,
    photo: String,
    status: Number
});

const UserSch = module.exports = mongoose.model('users', UserSchema);

module.exports.getUserById = function (id, callback) {
    UserSch.findById(id, callback);
}

module.exports.updateUser = function (user, callback) {
    const query = { _id: user._id }
    UserSch.update(query, user, callback);
}


module.exports.deleteUser = function (user, callback) {
    const query = { _id: user._id }
    UserSch.remove(query, user, callback);
}

module.exports.addUser = function (newUser, callback) {
    bcryptjs.genSalt(10, (err, salt) => {
        bcryptjs.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
}

module.exports.encryptPassword = function (password, callback) {
    bcryptjs.genSalt(10, (err, salt) => {
        bcryptjs.hash(password, salt, (err, hash) => {
            if (err) throw err;
            callback(null, hash);
        });
    });
}

module.exports.comparePassword = function (candidatePasword, hash, callback) {
    bcryptjs.compare(candidatePasword, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
}


module.exports.getUserByEmail = function (email, callback) {
    const query = { email: email }
    UserSch.findOne(query, callback);
}

