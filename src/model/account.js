var db = require('../config/db-connection')();
var mongoose = require('mongoose');


const AccountSchema = mongoose.Schema({
    user: String,
    accountNumber : String,
    currency: String,
    status: Number
});

const AccountSch = module.exports = mongoose.model('accounts', AccountSchema);


module.exports.getAccountsByID = function (id, callback) {
    AccountSch.findById(id, callback);
}

module.exports.getAccountsByUser = function (userId, callback) {
    const query = { user: userId }
    AccountSch.find(query, callback);
}
