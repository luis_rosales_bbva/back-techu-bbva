var db = require('../config/db-connection')();
var mongoose = require('mongoose');

const TransactionsSchema = mongoose.Schema({
    account: String,
    amount : Number,
    currency: String,
    operation: String,
    status: Number
});

const TransactionsSch = module.exports = mongoose.model('transactions', TransactionsSchema);


module.exports.getTransactionsByID = function (id, callback) {
    TransactionsSch.findById(id, callback);
}

module.exports.getTransactionsByAccount = function (accountId, callback) {
    const query = { account: accountId }
    TransactionsSch.find(query, callback);
}

module.exports.addTransactions = function (newTransactions, callback) {
    newTransactions.save(callback);
}