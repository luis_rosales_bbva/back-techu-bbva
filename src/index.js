const express = require('express');
const cors = require('cors');
const passport = require('passport'); 
var morgan = require('morgan')
const mongoose = require('mongoose');

//On Connection
mongoose.connection.on('connected', () => {
    console.log('Connected to database');
});
//Off Connection
mongoose.connection.on('error', (err) => {
    console.log('Database error : '+ err);
});

const app = express();

//Routes
const usersRoutes = require('./routes/users.js');
const AccountRoutes = require('./routes/accounts.js');

//Settings
app.set('port', process.env.PORT || 3000);


//Middlewares (Body Parse ya viene integrado en express 4.6.+)
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended : false}));
app.use(passport.initialize());
app.use(passport.session());
app.use(morgan("combined"));


//Passport init
require('./config/passport')(passport);


//Routes
app.use('/api/users', usersRoutes);
app.use('/api/accounts', AccountRoutes);


//Start Server
app.listen(app.get('port'),() => {
    console.log('Server on port ' + app.get('port'));
});
