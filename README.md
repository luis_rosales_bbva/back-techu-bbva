# Proyecto TechU - Practitioner

Proyecto de culminación de Curso - Nivel Practitioner. 

El siguiente parte de la plantilla de proyecto `Node JSt` como partida inicial.

Nuestro orquestador del proyecto utiliza las principales areas de trabajo

* **Mongo DB Atlas** MongoDB Atlas es el primer servicio de “Database as a Services” (DaS) para bases de datos MongoDB, en el cual es posible crear completos clusteres (Replica Set) de bases de datos, con unos cuantos click y exponerlas por internet para contactar nuestras aplicaciones si la necesidad de las tareas de instalación y administración.
* **Aws 2.773.0** Coneccion con amazon web services
* **Node Lts** Es un entorno en tiempo de ejecución multiplataforma, de código abierto, para la capa del servidor basado en el lenguaje de programación JavaScript
* **Express ** Framework multiplataforma 
* **bcryptjs** Parte de la encriptación 
* **Cors ** Manejador de Orígenes con la finalidad de que solo al ser llamados sólo puedan conectar al origen señalado.
* **jsonwebtoken** Maneja el tokenisado de seguridad al iniciar sesión
* **mongoose ** Driver que permite usar el mongo dentro de la librería express
* **morgan** Dependencia de desarrollo similar a un log que al ser llamada presenta el status y los dados de invocacion
* **multer ** Manipulador que permite subir archivos a amazon
* **object-hash** Permite crear llaves como hashes 
* **passport ** Encriptador que al recibir un token permite desencriptarlo // autentifica solicitudes
* **passport-jwt** Una estrategia de Passport para autenticarse con un JSON Web Token.
* **nodemon ** es una herramienta que ayuda a desarrollar aplicaciones basadas en node.js al reiniciar automáticamente la aplicación de nodo cuando se detectan cambios de archivo en el directorio.


### Configuración

##### Prerequisites


Instalar [npm](https://www.npmjs.com/) en la linea de comandos.

    npm install 

##### Inicializar proyecto

Ejecutar la siguiente linea de comando en el terminal // que ejecutara el index.JS

    npm run dev

